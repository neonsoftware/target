package com.software.neon.openchoose

import android.content.SharedPreferences

/**
 * Settings:
 * - is backed/up
 * - server TODO: remove if not used in the end
 *   - url
 *   - user
 *   - pass
 */
object SyncSettings {
    var isSync = true
    var serverUrl = ""
    var serverUser = ""
    var serverPass = ""

    fun load(prefs : SharedPreferences){
        SyncSettings.isSync = prefs.getBoolean("sync", true)
        SyncSettings.serverUrl = prefs.getString("url", "")
        SyncSettings.serverUser = prefs.getString("user", "")
        SyncSettings.serverPass = prefs.getString("pass", "")
    }

    fun store(prefs : SharedPreferences, newSettings: SyncSettings){
        val edit = prefs.edit()
        edit.putBoolean("sync", newSettings.isSync);
        edit.putString("url", newSettings.serverUrl);
        edit.putString("user", newSettings.serverUser);
        edit.putString("pass", newSettings.serverPass);
        edit.commit()
    }
}