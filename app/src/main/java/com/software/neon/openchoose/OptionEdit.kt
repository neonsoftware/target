package com.software.neon.openchoose

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import kotlinx.android.synthetic.main.fragment_option_edit.*
import com.software.neon.openchoose.databinding.FragmentOptionEditBinding


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_OPTION_ID = "option_id"
private const val ARG_OPTION_URL = "choice_url" //TODO unifoirm with option
private const val ARG_RESEARCH_ID = "research_id"

class OptionEdit : Fragment() {
    private var choicesVM : ChoicesViewModel? = null
    private var data = ChoiceData()
    private var vBinding : FragmentOptionEditBinding? =  null

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        if(item?.itemId == R.id.option_save){
            if (data.id == null){
                choicesVM?.insert(data)
            }else{
                choicesVM?.update(data)
            }
            vBinding?.root?.findNavController()?.navigate(R.id.action_optionEdit_pop)
            return true
        }else if(item?.itemId == R.id.option_delete){
            choicesVM?.delete(data)
            vBinding?.root?.findNavController()?.navigate(R.id.action_optionEdit_pop)
            return true
        }
        else{
            return super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if(activity is AppCompatActivity){
            (activity as AppCompatActivity).setSupportActionBar(option_edit_toolbar)
            (activity as AppCompatActivity).setTitle("Option")
        }
    }

    private var paramOptionId: Long? = null
    private var paramOptionUrl: String ? = null
    private var paramResearchId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            paramOptionId = if (it.containsKey(ARG_OPTION_ID)) it.getLong(ARG_OPTION_ID) else null
            paramOptionUrl = if (it.containsKey(ARG_OPTION_URL)) it.getString(ARG_OPTION_URL) else null
            paramResearchId = if (it.containsKey(ARG_RESEARCH_ID)) it.getString(ARG_RESEARCH_ID) else null
        }

        choicesVM = ViewModelProviders.of(this).get(ChoicesViewModel::class.java)

        setHasOptionsMenu(true)

        paramOptionId?.let {
            val a = choicesVM?.get(it)
            a?.observe(this, Observer {
                it?.let {
                    data = it
                    vBinding?.values = data
                }
            })
            return
        }

        paramResearchId?.let {
            val a = choicesVM?.getResearchInfo(it)
            a?.observe(this, Observer {
                it?.let {
                    // TODO add runtime toast error in case of empty return
                    data.researchId = it.id
                    data.researchType = it.type
                    data.researchName = it.name
                    data.url = paramOptionUrl ?: ""
                    vBinding?.values = data
                }
            })
            return
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        vBinding = FragmentOptionEditBinding.inflate(inflater)
        vBinding?.values = data
        vBinding?.labels = ChoiceTypes.types["person"]
        return vBinding?.root
    }


    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.option_edit, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

}
