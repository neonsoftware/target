package com.software.neon.openchoose

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.software.neon.openchoose.databinding.ChoiceCardPersonBinding
import com.software.neon.openchoose.databinding.ChoiceCardWorkBinding
import kotlinx.android.synthetic.main.fragment_options_list.*
import kotlinx.android.synthetic.main.fragment_researches_list.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_RESEARCH_ID = "research_id"
private const val ARG_RESEARCH_NAME = "research_name"
private const val ARG_RESEARCH_TYPE = "research_type"

class ChoiceNavigateHandlers (private val ch : ChoiceData?) {

    fun onClickFriend(view: View) {

        ch ?.let {
            var bundle = bundleOf("option_id" to it.id)
            view.findNavController().navigate(R.id.action_optionsList_to_optionEdit, bundle)

        }
    }
}

class OptionsList : Fragment() {
    // TODO: Rename and change types of parameters
    private var paramResearchId = ""
    private var paramResearchName = ""
    private var paramResearchType = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            paramResearchId = it.getString(ARG_RESEARCH_ID)
            paramResearchName = it.getString(ARG_RESEARCH_NAME)
            paramResearchType = it.getString(ARG_RESEARCH_TYPE)
        }
        setHasOptionsMenu(true)
    }

    private var choicesVM : ChoicesViewModel? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if(activity is AppCompatActivity){
            (activity as AppCompatActivity).setSupportActionBar(options_toolbar)
            (activity as AppCompatActivity).setTitle("Options")
        }

        options_recycler.layoutManager = LinearLayoutManager(context)

        choicesVM = ViewModelProviders.of(activity!!).get(ChoicesViewModel::class.java)

        choicesVM?.getChoices(paramResearchId)?.observe(this, Observer {
            it?.let {
                options_recycler.adapter = OptionsList.ChoicesListRecyclerViewAdapter(paramResearchType, choicesVM!!, it)
            }
        })

    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.researches, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return if(item.itemId == R.id.researches_add){
            var bundle = bundleOf("research_id" to paramResearchId)
            view?.findNavController()?.navigate(R.id.action_optionsList_to_optionEdit, bundle)
            true // true as "it was handled"
        }else{
            super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_options_list, container, false)
    }

    class ChoicesListRecyclerViewAdapter(private val researchType: String, var choicesVM : ChoicesViewModel, private val items : List<ChoiceData>) :
            RecyclerView.Adapter<ChoicesListRecyclerViewAdapter.ViewHolder>() {

        override fun getItemCount() = items.size

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

            var lLayout = R.layout.choice_card_work

            if(researchType == "work"){
                lLayout = R.layout.choice_card_work
            }else if (researchType == "person"){
                lLayout = R.layout.choice_card_person
            }

            return ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context), lLayout, parent, false))
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {

            if (holder.binding is ChoiceCardWorkBinding){
                holder.binding.act = null
                holder.binding.ch = items[position]
                holder.binding.choiceLabels = ChoiceTypes.types["person"]
                holder.binding.han = ChoiceNavigateHandlers(items[position])
                holder.binding.vm = choicesVM
            }else if(holder.binding is ChoiceCardPersonBinding){
                holder.binding.ch = items[position]
                holder.binding.han = ChoiceNavigateHandlers(items[position])
                holder.binding.vm = choicesVM
            }
        }

        inner class ViewHolder(bindingParam: ViewDataBinding) : RecyclerView.ViewHolder(bindingParam.root) {
            val binding = bindingParam
        }
    }

}
