package com.software.neon.openchoose

import androidx.lifecycle.LiveData
import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import com.google.gson.annotations.SerializedName
import java.util.*

data class CouchDBPostReponse(
        @SerializedName("id") val id: String,
        @SerializedName("rev") val rev: String,
        @SerializedName("ok") val ok: Boolean
)

data class ItemData(
        @SerializedName("_id") val id: String,
        @SerializedName("_rev") val rev: String,
        @SerializedName("Title") var title: String,
        @SerializedName("url") var url: String,
        @SerializedName("Zona") var zona: String,
        @SerializedName("prezzo") var prezzo: Int,
        @SerializedName("mq") var mq: Int,
        @SerializedName("num") var tel: String,
        @SerializedName("email") var email: String,
        @SerializedName("indirizzo") var indirizzo: String,
        @SerializedName("finestra") var finestra: String,
        @SerializedName("cave") var cave: String,
        @SerializedName("parking") var parking: String,
        @SerializedName("attiva") var attiva: Boolean,
        @SerializedName("verdura") var verdura: Int,
        @SerializedName("palazzons") var palazzons: Int,
        @SerializedName("trasporti") var trasporti: Int,
        @SerializedName("piano") var piano: Int,
        @SerializedName("ale") var ale: String,
        @SerializedName("cervs") var cervs: String
)

data class ItemRev(
        @SerializedName("rev") var rev: String
)

data class ItemBase(
        @SerializedName("id") val id: String,
        @SerializedName("key") val key: String,
        @SerializedName("value") val value: ItemRev,
        @SerializedName("doc") var doc: ItemData
)

data class ItemsCollection(
        @SerializedName("total_rows") val totalRows: Int,
        @SerializedName("offset") val offset: Int,
        @SerializedName("rows") val
        rows: List<ItemBase>
)

@Entity(tableName = "choiceData")
data class ChoiceData(@PrimaryKey(autoGenerate = true) var id: Long?,
                      @ColumnInfo(name = "isResearchDefinition") var isResearchDefinition: Boolean, // true when the entry is used just to store the existence of a research
                      @ColumnInfo(name = "researchId") var researchId: String,
                      @ColumnInfo(name = "researchType") var researchType: String,
                      @ColumnInfo(name = "researchName") var researchName: String,
                      @ColumnInfo(name = "active") var active: Boolean,
                      @ColumnInfo(name = "name") var name: String,
                      @ColumnInfo(name = "comment") var comment: String,
                      @ColumnInfo(name = "url") var url: String,
                      @ColumnInfo(name = "tel") var tel: String,
                      @ColumnInfo(name = "mail") var mail: String,
                      @ColumnInfo(name = "s1") var s1: String,
                      @ColumnInfo(name = "s2") var s2: String,
                      @ColumnInfo(name = "s3") var s3: String,
                      @ColumnInfo(name = "s4") var s4: String,
                      @ColumnInfo(name = "s5") var s5: String,
                      @ColumnInfo(name = "s6") var s6: String,
                      @ColumnInfo(name = "s7") var s7: String,
                      @ColumnInfo(name = "s8") var s8: String,
                      @ColumnInfo(name = "s9") var s9: String,
                      @ColumnInfo(name = "s10") var s10: String,
                      @ColumnInfo(name = "i1") var i1: Int,
                      @ColumnInfo(name = "i2") var i2: Int,
                      @ColumnInfo(name = "i3") var i3: Int,
                      @ColumnInfo(name = "i4") var i4: Int,
                      @ColumnInfo(name = "i5") var i5: Int,
                      @ColumnInfo(name = "i6") var i6: Int,
                      @ColumnInfo(name = "i7") var i7: Int,
                      @ColumnInfo(name = "i8") var i8: Int,
                      @ColumnInfo(name = "i9") var i9: Int,
                      @ColumnInfo(name = "i10") var i10: Int,
                      @ColumnInfo(name = "b1") var b1: Boolean,
                      @ColumnInfo(name = "b2") var b2: Boolean,
                      @ColumnInfo(name = "b3") var b3: Boolean,
                      @ColumnInfo(name = "b4") var b4: Boolean,
                      @ColumnInfo(name = "b5") var b5: Boolean,
                      @ColumnInfo(name = "r1") var r1: Int,
                      @ColumnInfo(name = "r2") var r2: Int,
                      @ColumnInfo(name = "r3") var r3: Int,
                      @ColumnInfo(name = "r4") var r4: Int,
                      @ColumnInfo(name = "r5") var r5: Int
){
    constructor():this(null, false, "","","",true, "", "","", "", "", "", "", "", "", "", "", "","","","", 0,0,0,0,0,0,0,0,0,0, false, false, false, false, false, 0,0,0,0,0)
}

data class ChoiceMeta(
      var type: String,
      var nameUsed: Boolean,
      var commentUsed: Boolean,
      var telUsed: Boolean,
      var urlUsed: Boolean,
      var mailUsed: Boolean,
      var s1Name: String,
      var s2Name: String,
      var s3Name: String,
      var s4Name: String,
      var s5Name: String,
      var s6Name: String,
      var s7Name: String,
      var s8Name: String,
      var s9Name: String,
      var s10Name: String,
      var i1Name: String,
      var i2Name: String,
      var i3Name: String,
      var i4Name: String,
      var i5Name: String,
      var i6Name: String,
      var i7Name: String,
      var i8Name: String,
      var i9Name: String,
      var i10Name: String,
      var b1Name: String,
      var b2Name: String,
      var b3Name: String,
      var b4Name: String,
      var b5Name: String,
      var r1Name: String,
      var r2Name: String,
      var r3Name: String,
      var r4Name: String,
      var r5Name: String
){
    constructor():this("", true, true, true, true, true,"", "", "", "", "",
            "", "", "", "", "",
            "", "", "", "", "",
            "", "", "", "", "",
            "", "", "", "", "",
            "", "", "", "", "")
}


object ChoiceTypes{
    var types : Map<String, ChoiceMeta> = mapOf(

            "work" to ChoiceMeta("",true, true, true, true, true,"Company", "Position",
                    "URL", "Contact Name", "Email", "Comment", "Location",
                    "Note", "", "",
                    "Salary", "Tel", "", "", "",
                    "", "", "", "", "",
                    "Remote", "Contacted", "Interviewed", "Ready", "",
                    "", "", "", "", ""),
            "person" to ChoiceMeta("",true, true, true, true, true,"Name", "Comment",
                    "URL", "Email", "Address", "", "",
                    "", "", "",
                    "Tel", "Price", "", "", "",
                    "", "", "", "", "",
                    "Contacted", "Approved", "Deal", "", "",
                    "", "", "", "", "")
    )
}

data class Research(
        @ColumnInfo(name = "researchId") var id: String,
        @ColumnInfo(name = "researchType") var type: String,
        @ColumnInfo(name = "researchName") var name: String
){
    constructor():this(UUID.randomUUID().toString(), "work","")
}

@Dao
interface ChoiceDataDAO {

    @Query("SELECT * from choiceData") //TODO probably now unused.
    fun getAll(): LiveData<List<ChoiceData>>

    @Query("SELECT * from choiceData WHERE ID = :id")
    fun get(id: Long): LiveData<ChoiceData>

    @Insert(onConflict = REPLACE)
    fun insert(weatherData: ChoiceData)

    @Update()
    fun update(weatherData: ChoiceData)

    @Delete()
    fun delete(weatherData: ChoiceData)

    @Query("DELETE from choiceData")
    fun deleteAll()

    @Query("SELECT * FROM choiceData WHERE researchId = :pResearchId AND isResearchDefinition = 0")
    fun getChoicesForResearch(pResearchId: String): LiveData<List<ChoiceData>>

    @Query("SELECT researchId,researchType,researchName FROM choiceData WHERE researchId = :pResearchId AND isResearchDefinition = 1")
    fun getResearchInfo(pResearchId: String): LiveData<Research>

    @Query("SELECT DISTINCT researchId,researchType,researchName from choiceData")
    fun getResearches(): LiveData<List<Research>>

}
