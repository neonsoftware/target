package com.software.neon.openchoose

import android.os.Bundle
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.view.*
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import com.software.neon.openchoose.databinding.SettingsDialogBinding

/**
 * The one and only activity. It deals with:
 * 1 - startup
 * 2 - bottom panel navigation
 * 3 - back button
 * 4 - settings
 */
class MainActivity : Home.OnFragmentInteractionListener, AppCompatActivity() {

    lateinit var choicesVM : ChoicesViewModel

    /**
     * Startup
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        when {
            intent?.action == Intent.ACTION_SEND -> {
                if ("text/plain" == intent.type) {
                    handleSendText(intent) // Handle text being sent
                }
            }
            else -> {
                // Handle other intents, such as being started from the home screen
                val y = 3 //
            }
        }


        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        choicesVM = ViewModelProviders.of(this).get(ChoicesViewModel::class.java)

        // call numbers
        choicesVM.toCall.observe(this, Observer {
            if (it != ""){
                val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:$it"))
                startActivity(intent)
                choicesVM.toCall.value = "" // reset value
            }
        })

        // send emails
        choicesVM.toMail.observe(this, Observer {
            if (it != ""){
                val intent = Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:$it"))
                startActivity(intent)
                choicesVM.toMail.value = "" // reset value
            }
        })

        // visit webpage
        choicesVM.toSurf.observe(this, Observer {
            if (it != ""){
                var url = it
                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "http://" + url;
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                startActivity(intent)
                choicesVM.toSurf.value = "" // reset value
            }
        })

        refreshSettings()
    }

    private fun handleSendText(intent: Intent) {
        intent.getStringExtra(Intent.EXTRA_TEXT)?.let {
            var bundle = bundleOf("add_url" to it)
            findNavController(R.id.nav_host_fragment).navigate(R.id.researchesList, bundle)
        }
    }


    /**
     * Bottom panel navigation.
     */
    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                findNavController(R.id.nav_host_fragment).navigate(R.id.homepage)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_dashboard -> {
                findNavController(R.id.nav_host_fragment).navigate(R.id.researchesList)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_notifications -> {
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    /**
     * CLiecking the "back" button
     */
    override fun onSupportNavigateUp()
            = findNavController(R.id.nav_host_fragment).navigateUp()


    /**
     * Settings - retrieve
     */
    private fun refreshSettings():Boolean{
        SyncSettings.load(getPreferences(MODE_PRIVATE))
        ItemsRepository.refreshWebserver(SyncSettings.serverUrl)
        return true
    }

    /**
     * Settings - edit
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        //if(item.itemId == R.id.action_settings){
        if(false){
            val dialogBuilder = AlertDialog.Builder(this)
            val binding = SettingsDialogBinding.inflate(this.layoutInflater)

            binding.boundSettingVar = SyncSettings
            dialogBuilder.setView(binding.root)
            dialogBuilder.setTitle(item.title)
            dialogBuilder.setMessage("Edit")
            dialogBuilder.setPositiveButton("Save") { _, _ ->
                binding.boundSettingVar?.let {
                    SyncSettings.store(getPreferences(MODE_PRIVATE), it)
                }
            }
            dialogBuilder.setNegativeButton("Discard") { _, _ ->
                binding.boundSettingVar?.let {  }
            }
            val dialog = dialogBuilder.create()
            dialog.show()

            return true // true as "it was handled"
        }else{
            return super.onOptionsItemSelected(item)
        }
    }

    /**
     * Callback, not needed yet.
     */
    override fun onFragmentInteraction(uri: Uri) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
