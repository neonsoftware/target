package com.software.neon.openchoose

import android.app.Application
import androidx.lifecycle.*

/**
 * TODO Unused. Could be removed soon.
 */
class ItemsListViewModel : ViewModel() {

    var items : MutableLiveData<MutableMap<String, ItemData>>? = null

    init {
        if (this.items != null) {
            // ViewModel is created per Fragment so
            // we know the userId won't change
        }else{
            items = ItemsRepository.getItems()
        }
    }

    fun create(newItem : ItemData) {
        ItemsRepository.webservice?.createItem(newItem)?.enqueue(CouchDBResponseHandler)
    }

    fun update(id: String, newItem : ItemData) {
        items?.value?.get(id)?.let { ItemsRepository.webservice?.updateItem(it.id, newItem)?.enqueue(CouchDBResponseHandler) }
    }

    fun delete(id: String) {
        items?.value?.get(id)?.let { ItemsRepository.webservice?.deleteItem(it.id)?.enqueue(CouchDBResponseHandler) }
    }

    fun filteredList() : List<ItemData>{

        val allItemsList = mutableListOf<ItemData>()

        items?.value?.let {
            val actives = mutableListOf<ItemData>()
            val inactives = mutableListOf<ItemData>()

            for (item in it.toList()) {
                if (item.second.attiva)
                    actives.add(item.second)
                else
                    inactives.add(item.second)
            }
            allItemsList.addAll(actives+ inactives)
        }

        return allItemsList
    }
}

/**
 * Proxy for data repository, but inheriting AndroidViewModel life-cycle (remains alive).
 * UX uses this as handle, as it survives ux death.
 */
class ChoicesViewModel(application: Application) : AndroidViewModel(application) {

    /* observed by activity, triggered by everyone, everywhere*/
    var toCall: MutableLiveData<String> = MutableLiveData()
    var toMail: MutableLiveData<String> = MutableLiveData()
    var toSurf: MutableLiveData<String> = MutableLiveData()

    var allWords: LiveData<List<ChoiceData>>? = null
    var researches: LiveData<List<Research>>? = null
    private val mRepository : ChoicesRepository = ChoicesRepository(application)

    init {
        allWords = mRepository.allWords
        researches = mRepository.researches
    }

    fun get(id: Long) : LiveData<ChoiceData>? {
        return mRepository.get(id)
    }

    fun insert(word: ChoiceData) {
        mRepository.insert(word)
    }

    fun update(word: ChoiceData) {
        mRepository.update(word)
    }

    fun delete(word: ChoiceData) {
        mRepository.delete(word)
    }

    fun getChoices(pResearchId: String) :LiveData<List<ChoiceData>>?  {
        return mRepository.getChoices(pResearchId)
    }

    fun getResearchInfo(pResearchId: String) :LiveData<Research>?  {
        return mRepository.getResearchInfo(pResearchId)
    }

    fun call(number: String) {
        toCall.postValue(number)
    }

    fun mail(address: String) {
        toMail.postValue(address)
    }

    fun surf(url: String) {
        toSurf.postValue(url)
    }

    fun insertResearch(pResearch: Research) {
        val dummy = ChoiceData()
        dummy.isResearchDefinition = true
        dummy.researchId = pResearch.id
        dummy.researchType = pResearch.type
        dummy.researchName = pResearch.name
        insert(dummy)
    }

    fun deleteResearch(pResearch: Research) {
        // TODO. remove all choices. Should ask for confirmation..
    }
}

