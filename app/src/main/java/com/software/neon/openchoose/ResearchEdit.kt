package com.software.neon.openchoose

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import com.software.neon.openchoose.databinding.FragmentResearchEditBinding
import kotlinx.android.synthetic.main.fragment_option_edit.*

private const val ARG_RESEARCH_ID = "research_id"

class ResearchEdit : Fragment() {
    private var paramResearchId: String? = null
    private var choicesVM : ChoicesViewModel? = null
    private var mResearch = Research()
    private var vBinding : FragmentResearchEditBinding? =  null


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if(activity is AppCompatActivity){
            (activity as AppCompatActivity).setSupportActionBar(option_edit_toolbar)
            (activity as AppCompatActivity).setTitle("Research")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            paramResearchId = it.getString(ARG_RESEARCH_ID)
        }

        choicesVM = ViewModelProviders.of(this).get(ChoicesViewModel::class.java)

        paramResearchId?.let {
            //val a = choicesVM?.get(it)
            //a?.observe(this, Observer {
            //    it?.let {
            //        data = it
            //        vBinding?.values = data
            //    }
            //})
        }

        setHasOptionsMenu(true)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        return when {
            item?.itemId == R.id.option_save -> {
                mResearch.type = ChoiceTypes.types.toList()[vBinding!!.typePosition!!].first
                choicesVM?.insertResearch(mResearch)
                vBinding?.root?.findNavController()?.navigate(R.id.action_researchEdit_pop)
                true
            }
            item?.itemId == R.id.option_delete -> {
                choicesVM?.deleteResearch(mResearch)
                vBinding?.root?.findNavController()?.navigate(R.id.action_researchEdit_pop)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        vBinding = FragmentResearchEditBinding.inflate(inflater)
        vBinding?.research = mResearch
        vBinding?.typePosition = ChoiceTypes.types.keys.indexOf(mResearch.type)

        return vBinding?.root
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.option_edit, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

}
