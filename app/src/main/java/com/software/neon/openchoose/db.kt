package com.software.neon.openchoose

import android.content.Context
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import android.os.AsyncTask
import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase


@Database(entities = [ChoiceData::class], version = 1)
abstract class ChoicesDataBase : RoomDatabase() {

    abstract fun choiceDataDao(): ChoiceDataDAO

    companion object {
        private var INSTANCE: ChoicesDataBase? = null

        fun getInstance(context: Context): ChoicesDataBase? {
            if (INSTANCE == null) {
                synchronized(ChoicesDataBase::class) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            ChoicesDataBase::class.java, "choices.db")
                            .build()
                }
            }
            return INSTANCE
        }

        fun getZaInstance(): ChoicesDataBase? {
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}


interface ServerItemsApi {
    @GET("" +
            "casette/_all_docs?include_docs=true")
    fun getItems(): Call<ItemsCollection>

    @POST("casette")
    fun createItem(@Body item: ItemData): Call<CouchDBPostReponse>

    @GET("casette/{id}")
    fun getItem(@Path("id") userId: String): Call<ItemData>

    @PUT("casette/{id}")
    fun updateItem(@Path("id") userId: String, @Body item: ItemData): Call<CouchDBPostReponse>

    @DELETE("casette/{id}")
    fun deleteItem(@Path("id") userId: String): Call<CouchDBPostReponse>

    companion object {
        fun create(url:String): ServerItemsApi = Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(ServerItemsApi::class.java)
    }
}

object ItemsRepository {

    var webservice :ServerItemsApi? = null

    fun refreshWebserver(url:String) {
        webservice = try { ServerItemsApi.create(url) } catch (e: IllegalArgumentException) { null }
    }

    fun getItems(): MutableLiveData<MutableMap<String, ItemData>> {

        val data = MutableLiveData<MutableMap<String, ItemData>>()
        webservice?.getItems()?.enqueue(object : Callback<ItemsCollection> {
            override fun onFailure(call: Call<ItemsCollection>?, t: Throwable?) {
                println("Nuuu, problema")
            }

            override fun onResponse(call: Call<ItemsCollection>, response: Response<ItemsCollection>) {
                response.body()?.let {
                    val tempMap = mutableMapOf<String, ItemData>()
                    for(row in it.rows)
                    {
                        tempMap[row.id] = row.doc
                    }
                    data.value = tempMap
                }
            }
        })
        return data
    }
}

class ChoicesRepository internal constructor(application: Application) {

    var allWords: LiveData<List<ChoiceData>>? = null
    var researches: LiveData<List<Research>>? = null
    var mWordDao: ChoiceDataDAO?

    init {
        val db = ChoicesDataBase.getInstance(application)
        mWordDao = db?.choiceDataDao()
        mWordDao?.getAll()?.let{
            allWords = it
        }
        mWordDao?.getResearches()?.let{
            researches = it
        }
    }

    fun getChoices(pResearchId: String) : LiveData<List<ChoiceData>>?{
        mWordDao?.let {
            return it.getChoicesForResearch(pResearchId)
        }
        return null
    }

    fun getResearchInfo(pResearchId: String) : LiveData<Research>?{
        mWordDao?.let {
            return it.getResearchInfo(pResearchId)
        }
        return null
    }

    fun get(id: Long) : LiveData<ChoiceData>?{
        mWordDao?.let {
            return it.get(id)
        }
        return null
    }

    fun insert(word: ChoiceData) {
        mWordDao?.let {
            insertAsyncTask(it).execute(word)
        }
    }

    fun update(word: ChoiceData) {
        mWordDao?.let {
            updateAsyncTask(it).execute(word)
        }
    }

    fun delete(word: ChoiceData) {
        mWordDao?.let {
            deleteAsyncTask(it).execute(word)
        }
    }

    private class insertAsyncTask internal constructor(private val mAsyncTaskDao: ChoiceDataDAO) : AsyncTask<ChoiceData, Void, Void>() {

        override fun doInBackground(vararg params: ChoiceData): Void? {
            mAsyncTaskDao.insert(params[0])
            return null
        }
    }

    private class updateAsyncTask internal constructor(private val mAsyncTaskDao: ChoiceDataDAO) : AsyncTask<ChoiceData, Void, Void>() {

        override fun doInBackground(vararg params: ChoiceData): Void? {
            mAsyncTaskDao.update(params[0])
            return null
        }
    }

    private class deleteAsyncTask internal constructor(private val mAsyncTaskDao: ChoiceDataDAO) : AsyncTask<ChoiceData, Void, Void>() {

        override fun doInBackground(vararg params: ChoiceData): Void? {
            mAsyncTaskDao.delete(params[0])
            return null
        }
    }
}


object CouchDBResponseHandler : Callback<CouchDBPostReponse> {
    override fun onFailure(call: Call<CouchDBPostReponse>?, t: Throwable?) {
        println("Nuuu, problema")
    }

    override fun onResponse(call: Call<CouchDBPostReponse>, response: Response<CouchDBPostReponse>) {
        println("Fattoooo")
    }
}