package com.software.neon.openchoose

import android.app.AlertDialog
import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.software.neon.openchoose.databinding.ChoiceEditBinding
import com.software.neon.openchoose.databinding.ResearchCardBinding
import kotlinx.android.synthetic.main.fragment_researches_list.*

private const val ARG_OPEN_URL_ID = "add_url"
var eventualUrlToTopen : String ? = null //TODO remove as global variable

// TODO remove as external class
class NavigateHandlers (private val ch : Research?) {

    fun itsAboutAddingUrl():Boolean{
        return eventualUrlToTopen != null
    }

    fun onClickFriend(view: View) {

        if(itsAboutAddingUrl()){

            ch ?.let {
                var bundle = bundleOf("research_id" to ch.id, "choice_url" to eventualUrlToTopen)
                view.findNavController().navigate(R.id.action_researchesList_to_optionEdit, bundle)
            }
        }else{
            ch ?.let {
                var bundle = bundleOf("research_id" to ch.id, "research_type" to ch.type, "research_name" to ch.name)
                view.findNavController().navigate(R.id.action_researchesList_to_optionsList, bundle)
            }
        }

    }
}

class ResearchesList : Fragment() {

    private var choicesVM : ChoicesViewModel? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if(activity is AppCompatActivity){
            (activity as AppCompatActivity).setSupportActionBar(researches_toolbar)
            (activity as AppCompatActivity).setTitle("Researches")
        }

        items_recycle.layoutManager = LinearLayoutManager(context)

        choicesVM = ViewModelProviders.of(activity!!).get(ChoicesViewModel::class.java)

        choicesVM?.researches?.observe(this, Observer {
            it?.let {
                items_recycle.adapter = ResearchesListRecyclerViewAdapter(choicesVM!!, it)
            }
        })

    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.researches, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return if(item.itemId == R.id.researches_add){
            view?.findNavController()?.navigate(R.id.action_researchesList_to_researchEdit)
            true // true as "it was handled"
        }else{
            super.onOptionsItemSelected(item)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            eventualUrlToTopen = it.getString(ARG_OPEN_URL_ID)
        }

        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_researches_list, container, false)
    }

    class ResearchesListRecyclerViewAdapter(var choicesVM : ChoicesViewModel, private val items : List<Research>) :
            RecyclerView.Adapter<ResearchesListRecyclerViewAdapter.ViewHolder>() {

        override fun getItemCount() = items.size

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val binding : ResearchCardBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.research_card, parent, false)
            return ViewHolder(binding)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {

            holder.binding.research = items[position]
            holder.binding.han = NavigateHandlers(items[position])
        }

        inner class ViewHolder(bindingParam: ResearchCardBinding) : RecyclerView.ViewHolder(bindingParam.root) {
            val binding = bindingParam
        }
    }
}
