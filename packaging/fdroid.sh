START_DIR=$PWD
cd $(mktemp -d)
git clone https://gitlab.com/fdroid/fdroiddata.git
git clone https://gitlab.com/fdroid/fdroidserver.git
cp $START_DIR/com.software.neon.openchoose.yml fdroiddata/metadata
export PATH=$PWD/fdroidserver:$PATH
cp fdroidserver/gradlew-fdroid fdroidserver/gradle
cd fdroiddata
export ANDROID_HOME=~/Android/Sdk
fdroid build -l -v com.software.neon.openchoose
